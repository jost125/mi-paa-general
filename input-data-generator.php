#!/usr/bin/php
<?php

if (count ($argv) != 9) {
	die ("Usage: $argv[0] numberOfInstances numberOfItemsInInstace minKnapsackWeight maxKnapsackWeight minItemWeight maxItemWeight minItemPrice maxItemPrice\n");
}

list (
	$foo, $numberOfInstances, $numberOfItems, $minKnapsackWeight, $maxKnapsackWeight,
	$minItemWeight, $maxItemWeight, $minItemPrice, $maxItemPrice
) = $argv;
for ($id = 0; $id < $numberOfInstances; $id++) {
	echo "$id $numberOfItems " . rand($minKnapsackWeight, $maxKnapsackWeight);
	for ($itemNumber = 0; $itemNumber < $numberOfItems; $itemNumber++) {
		echo " " . rand($minItemWeight, $maxItemWeight) . " " . rand($minItemPrice, $maxItemPrice);
	}
	echo "\n";
}

