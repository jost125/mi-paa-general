#!/bin/bash

FILE=$1
ALGORITHM=$2
CMD=$(ls dist/ | grep .jar)
if [ ! $2 ]
then
	java -jar dist/$CMD $FILE | awk 'BEGIN { FS=";"} { print $4 " " $7 }' | awk 'BEGIN { bruteForceTimesSum = 0; heuresticTimesSum = 0; numberOfInstances = 0; } { numberOfInstances++; bruteForceTimesSum += $3; heuresticTimesSum += $6; } END { print "brute force time: " bruteForceTimesSum / numberOfInstances " heurestic time: " heuresticTimesSum / numberOfInstances }'
else
	java -jar dist/$CMD $FILE $ALGORITHM | awk 'BEGIN { FS=";"} { print $4 }' | awk 'BEGIN { timesSum = 0; numberOfInstances = 0; } { numberOfInstances++; timesSum += $3; } END { print "time: " timesSum / numberOfInstances }'
fi


