#!/bin/bash
FILE=$1
CMD=$(ls dist/ | grep .jar)

java -jar dist/$CMD $FILE | awk 'BEGIN { FS=";" } { print $3 " " $6 }' | awk 'BEGIN { numberOfInstances=0; errorSum=0; errorMax=0 } { numberOfInstances++; if ($3 != 0) { error=(($3-$6)/($3)) } else { error=0 }; errorSum+=error; if (error > errorMax) {errorMax = error} } END { print "avg error: " errorSum / numberOfInstances " max error: " errorMax }'
