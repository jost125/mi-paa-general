#!/bin/bash
FILE=$1
CMD=$(ls dist/ | grep .jar)

java -jar dist/$CMD "knapsack" $FILE | awk 'BEGIN { FS=";" } { if ($2 != $5) print "different " $1 " " $2 " " $3 " " $5 " " $6 }'
