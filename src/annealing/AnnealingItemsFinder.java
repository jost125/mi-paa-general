package annealing;

import cz.cvut.felk.cig.jcop.algorithm.Algorithm;
import cz.cvut.felk.cig.jcop.problem.knapsack.Knapsack;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackInstance;
import cz.cvut.felk.cig.jcop.result.render.JFreeChartRender;
import cz.cvut.felk.cig.jcop.solver.SimpleSolver;
import cz.cvut.felk.cig.jcop.solver.Solver;
import knapsack.BestKnapsackConfigurationRender;

public class AnnealingItemsFinder {
	public void getSolution(KnapsackInstance knapsackInstance) {
		Knapsack knapsackProblem = new Knapsack(knapsackInstance);

		double startingTemperature = 500.0;
		double annealingCoeaficient = 0.85;
		int numberOfLoopsBeforeAnnealing = 10000;
		double frozenTemperature = 5.0;

		Algorithm algorithm = new SimulatedAnnealing(startingTemperature, annealingCoeaficient, numberOfLoopsBeforeAnnealing, frozenTemperature);

		Solver solver = new SimpleSolver(algorithm, knapsackProblem);
		solver.addRender(new BestKnapsackConfigurationRender());
		solver.addListener(new JFreeChartRender("Demo JFreeChartRender"));
		solver.run();
		solver.render();
	}
}
