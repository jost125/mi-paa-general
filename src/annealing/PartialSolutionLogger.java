package annealing;

import cz.cvut.felk.cig.jcop.solver.message.Message;
import cz.cvut.felk.cig.jcop.solver.message.MessageBetterConfigurationFound;
import cz.cvut.felk.cig.jcop.solver.message.MessageListener;
import cz.cvut.felk.cig.jcop.solver.message.MessageOptimize;
import cz.cvut.felk.cig.jcop.solver.message.MessageSolverStart;
import cz.cvut.felk.cig.jcop.solver.message.MessageSolverStop;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import logging.FileLogger;

public class PartialSolutionLogger implements MessageListener {

	protected int optimizeCounter = 0;
	private Map<Integer, Double> configurations;
	private FileLogger fileLogger;

	public PartialSolutionLogger(FileLogger fileLogger) {
		this.fileLogger = fileLogger;
	}

	@Override
	public void onMessage(Message message) {
		if (message instanceof MessageSolverStart) {
			optimizeCounter = 0;
			configurations = new HashMap<Integer, Double>();
		} else if (message instanceof MessageOptimize) {
			optimizeCounter++;
		} else if (message instanceof MessageBetterConfigurationFound) {
			double fitness = ((MessageBetterConfigurationFound) message).getFitness();
			configurations.put(optimizeCounter, fitness);
		} else if (message instanceof MessageSolverStop) {
			logConfigurations();
		}
	}

	private void logConfigurations() {
		Set<Integer> optimizeCounters = configurations.keySet();
		for (Integer i : optimizeCounters) {
			Double fitness = configurations.get(i);
			this.fileLogger.log(i + "\t" + fitness);
		}
	}
}
