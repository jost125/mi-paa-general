package bucket;

import core.CannotLoadInstanceException;
import cz.cvut.felk.cig.jcop.problem.bucket.BucketInstance;
import cz.cvut.felk.cig.jcop.problem.bucket.BucketTokenizer;
import java.io.BufferedReader;
import java.io.IOException;

public class BucketLoader {

	private BucketTokenizer bucketTokenizer;

	public BucketLoader(BucketTokenizer bucketTokenizer) {
		this.bucketTokenizer = bucketTokenizer;
	}

	public BucketInstance loadNextInstance(BufferedReader bufferedReader) throws CannotLoadInstanceException {
		try {
			String InstanceString = bufferedReader.readLine();
			BucketInstance knapsackInstance = null;
			if (InstanceString != null) {
				knapsackInstance = bucketTokenizer.getInstance(InstanceString);
			}
			return knapsackInstance;
		} catch (IOException ex) {
			throw new CannotLoadInstanceException("Load of knapsack instance failed", ex);
		}
	}
}
