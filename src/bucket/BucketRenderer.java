package bucket;

import cz.cvut.felk.cig.jcop.problem.OperationHistory;
import cz.cvut.felk.cig.jcop.result.Result;
import cz.cvut.felk.cig.jcop.result.ResultEntry;
import cz.cvut.felk.cig.jcop.result.render.Render;
import java.io.IOException;
import java.util.List;

public class BucketRenderer implements Render {

	@Override
	public void render(Result result) throws IOException {
		ResultEntry resultEntry = result.getResultEntries().get(0);
		List<OperationHistory> chronologicalList = resultEntry.getBestConfiguration().getOperationHistory().getChronologicalList();
		System.out.print("Nodes: " + resultEntry.getOptimizeCounter() + " ");
		System.out.println("Length: " + chronologicalList.size());
	}

}
