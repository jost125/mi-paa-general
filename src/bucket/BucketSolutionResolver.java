package bucket;

import bucket.bfs.BFSPathFinder;
import bucket.heuristic.HeuristicPathFinder;
import core.CannotLoadInstanceException;
import core.Loader;
import core.SolutionResolver;
import cz.cvut.felk.cig.jcop.problem.bucket.BucketInstance;
import java.io.BufferedReader;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class BucketSolutionResolver implements SolutionResolver {

	private Loader bucketLoader;
	private BFSPathFinder bFSPathFinder;
	private HeuristicPathFinder heuristicPathFinder;

	public BucketSolutionResolver(Loader bucketLoader, BFSPathFinder bfsPathFinder, HeuristicPathFinder heuristicPathFinder) {
		this.bucketLoader = bucketLoader;
		this.bFSPathFinder = bfsPathFinder;
		this.heuristicPathFinder = heuristicPathFinder;
	}

	@Override
	public void run(BufferedReader bufferedReader, Map<String, Object> options) throws CannotLoadInstanceException {
		Logger.getRootLogger().setLevel(Level.OFF);

		BucketInstance bucketInstance = (BucketInstance)bucketLoader.loadNextInstance(bufferedReader);
		while (bucketInstance != null) {
			System.out.print("id: ");
			System.out.print(bucketInstance.getId() + " ");
			if (options.containsKey("shouldRunBruteForce")) {
				bFSPathFinder.getSolution(bucketInstance);
			}

			System.out.print("id: ");
			System.out.print(bucketInstance.getId() + " ");
			if (options.containsKey("shouldRunHeuristic")) {
				heuristicPathFinder.getSolution(bucketInstance);
			}
			bucketInstance = (BucketInstance)bucketLoader.loadNextInstance(bufferedReader);
		}
	}
}
