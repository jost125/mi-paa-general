package bucket.bfs;

import bucket.BucketRenderer;
import cz.cvut.felk.cig.jcop.algorithm.Algorithm;
import cz.cvut.felk.cig.jcop.algorithm.graphsearch.bfs.BreadthFirstSearch;
import cz.cvut.felk.cig.jcop.problem.bucket.Bucket;
import cz.cvut.felk.cig.jcop.problem.bucket.BucketInstance;
import cz.cvut.felk.cig.jcop.solver.SimpleSolver;
import cz.cvut.felk.cig.jcop.solver.Solver;
import cz.cvut.felk.cig.jcop.solver.condition.FoundSolutionCondition;

public class BFSPathFinder {
	public void getSolution(BucketInstance bucketInstace) {
		Bucket bucketProblem = new Bucket(
			bucketInstace.getCapacities(),
			bucketInstace.getStartingContents(),
			bucketInstace.getDestinationContents()
		);

		Algorithm algorithm = new BreadthFirstSearch();

		Solver solver = new SimpleSolver(algorithm, bucketProblem);
		solver.addRender(new BucketRenderer());
		solver.addStopCondition(new FoundSolutionCondition());

		solver.run();
		solver.render();
	}
}
