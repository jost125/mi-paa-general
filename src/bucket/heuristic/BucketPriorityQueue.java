package bucket.heuristic;

import bucket.heuristic.comparator.ManhattanBucketComparator;
import cz.cvut.felk.cig.jcop.algorithm.graphsearch.GraphSearchQueue;
import cz.cvut.felk.cig.jcop.problem.Configuration;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class BucketPriorityQueue implements GraphSearchQueue {

	protected Queue<Configuration> queue;
	protected Set<List<Integer>> set;

	/**
	 * Creates new Queue for BFS.
	 */
	public BucketPriorityQueue(Configuration destinationConfiguration) {
//		this.queue = new PriorityQueue<Configuration>(10, new EuklidBucketComparator(destinationConfiguration));
		this.queue = new PriorityQueue<Configuration>(10, new ManhattanBucketComparator(destinationConfiguration));
		this.set = new HashSet<List<Integer>>();
	}

	@Override
	public Configuration fetch() {
		return queue.poll();
	}

	@Override
	public void add(Configuration newElement) {
		queue.add(newElement);
		set.add(newElement.asList());
	}

	@Override
	public boolean testPresence(Configuration testedElement) {
		return set.contains(testedElement.asList());
	}

	@Override
	public int size() {
		return queue.size();
	}
}
