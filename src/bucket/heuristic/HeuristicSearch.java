package bucket.heuristic;

import cz.cvut.felk.cig.jcop.algorithm.graphsearch.GraphSearchQueue;
import cz.cvut.felk.cig.jcop.algorithm.graphsearch.bfs.BreadthFirstSearch;

public class HeuristicSearch extends BreadthFirstSearch {

	@Override
	protected GraphSearchQueue initQueue() {
		return new BucketPriorityQueue(problem.getDestinations().get(0));
	}
}
