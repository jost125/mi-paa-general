package bucket.heuristic.comparator;

import cz.cvut.felk.cig.jcop.problem.Configuration;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class EuklidBucketComparator implements Comparator<Configuration> {

	private Configuration destinationConfiguration;

	public EuklidBucketComparator(Configuration destinationConfiguration) {
		this.destinationConfiguration = destinationConfiguration;
	}

	@Override
	public int compare(Configuration t1, Configuration t2) {
		List<Integer> cList1 = t1.asList();
		List<Integer> cList2 = t2.asList();
		List<Integer> destinationList = destinationConfiguration.asList();

		Double list1Similarity = listSimilarity(cList1, destinationList);
		Double list2Similarity = listSimilarity(cList2, destinationList);

		return list1Similarity.compareTo(list2Similarity);
	}

	private double listSimilarity(List<Integer> list1, List<Integer> list2) {
		Iterator<Integer> iterator1 = list1.iterator();
		Iterator<Integer> iterator2 = list2.iterator();

		int sum = 0;

		while (iterator1.hasNext()) {
			sum += Math.pow(iterator1.next() - iterator2.next(), 2);
		}

		return Math.pow(sum, 0.5);
	}

}
