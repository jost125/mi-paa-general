package bucket.heuristic.comparator;

import cz.cvut.felk.cig.jcop.problem.Configuration;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ManhattanBucketComparator implements Comparator<Configuration> {

	private Configuration destinationConfiguration;

	public ManhattanBucketComparator(Configuration destinationConfiguration) {
		this.destinationConfiguration = destinationConfiguration;
	}

	@Override
	public int compare(Configuration c1, Configuration c2) {
		List<Integer> cList1 = c1.asList();
		List<Integer> cList2 = c2.asList();
		List<Integer> destinationList = destinationConfiguration.asList();

		Integer manhattanDistance1 = manhattanDistance(cList1, destinationList);
		Integer manhattanDistance2 = manhattanDistance(cList2, destinationList);

		return manhattanDistance1.compareTo(manhattanDistance2);
	}

	private int manhattanDistance(List<Integer> vector1, List<Integer> vector2) {
		Iterator<Integer> iterator1 = vector1.iterator();
		Iterator<Integer> iterator2 = vector2.iterator();

		int sum = 0;

		while (iterator1.hasNext()) {
			sum += Math.abs(iterator1.next() - iterator2.next());
		}

		return sum;
	}

}
