package core;

public class ArgumentsMarshaler {

	private String[] args;

	public ArgumentsMarshaler(String[] args) {
		this.args = args;
	}

	public void checkNumberOfArguments(int min, int max) throws WrongArgumentsException {
		if (args.length < min) {
			throw new TooFewArgumentsException("Too few arguments");
		} else if (args.length > max) {
			throw new TooManyArgumentsException("Too many arguments");
		}
	}

	public String getArgument(int index) {
		return args[index];
	}
	
}
