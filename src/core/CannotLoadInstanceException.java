package core;

import java.io.IOException;

public class CannotLoadInstanceException extends Exception {

	public CannotLoadInstanceException(String message, IOException ex) {
		super(message, ex);
	}

}
