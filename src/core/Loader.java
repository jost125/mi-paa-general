package core;

import core.CannotLoadInstanceException;
import cz.cvut.felk.cig.jcop.instance.IInstance;
import cz.cvut.felk.cig.jcop.util.ITokenizer;
import java.io.BufferedReader;
import java.io.IOException;

public class Loader {

	private ITokenizer tokenizer;

	public Loader(ITokenizer tokenizer) {
		this.tokenizer = tokenizer;
	}

	public IInstance loadNextInstance(BufferedReader bufferedReader) throws CannotLoadInstanceException {
		try {
			String instanceString = bufferedReader.readLine();
			IInstance instance = null;
			if (instanceString != null) {
				instance = tokenizer.getInstance(instanceString);
			}
			return instance;
		} catch (IOException ex) {
			throw new CannotLoadInstanceException("Load of instance failed", ex);
		}
	}
}
