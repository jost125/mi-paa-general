package core;

import annealing.AnnealingItemsFinder;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackTokenizer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import knapsack.KnapsackSolutionResolver;
import knapsack.branchbound.BBItemsFinder;
import knapsack.bfs.BFSItemsFinder;
import knapsack.dynamic.DynamicItemsFinder;
import knapsack.ftpas.FPTASItemsFinder;
import knapsack.heuristic.HeuristicItemsFinder;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main {

	public static void main(String[] args) {
		Logger.getRootLogger().setLevel(Level.ERROR);

		ArgumentsMarshaler argumentsMarshaler = new ArgumentsMarshaler(args);
		KnapsackSolutionResolver knapsackSolutionResolver = new KnapsackSolutionResolver(
			new Loader(new KnapsackTokenizer()),
			new BFSItemsFinder(),
			new HeuristicItemsFinder(),
			new BBItemsFinder(),
			new DynamicItemsFinder(),
			new FPTASItemsFinder(4),
			new AnnealingItemsFinder()
		);

		try {
			argumentsMarshaler.checkNumberOfArguments(2, 2);
			Map<String, Object> options = new HashMap<String, Object>();
			String algorithmType = (String)argumentsMarshaler.getArgument(0);
			if (!algorithmType.matches("bfs|heuristic|bb|dynamic|fptas|ann")) {
				throw new UnknownAlgorithmException("Unknown algorithm" + algorithmType);
			}
			
			options.put(algorithmType, true);

			knapsackSolutionResolver.run(
				getBufferedReader(argumentsMarshaler.getArgument(1)),
				options
			);
		} catch (FileNotFoundException fileNotFoundException) {
			System.err.println(fileNotFoundException.getMessage());
		} catch (CannotLoadInstanceException exception) {
			System.err.println(exception.getMessage());
		} catch (WrongArgumentsException exception) {
			System.err.println(exception.getMessage());
		} catch (UnknownAlgorithmException exception) {
			System.err.println(exception.getMessage());
		}
	}

	public static BufferedReader getBufferedReader(String fileName) throws FileNotFoundException {
		File file = new File(fileName);
		if (!file.exists() || !file.canRead()) {
			throw new FileNotFoundException("File " + fileName + " does not exists or is not readable.");
		}
		return new BufferedReader(new FileReader(file));
	}
}
