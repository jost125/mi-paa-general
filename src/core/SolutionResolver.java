package core;

import java.io.BufferedReader;
import java.util.Map;

public interface SolutionResolver {
	public void run(BufferedReader bufferedReader, Map<String, Object> options) throws CannotLoadInstanceException;
}
