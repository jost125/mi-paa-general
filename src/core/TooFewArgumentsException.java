package core;

class TooFewArgumentsException extends WrongArgumentsException {

	public TooFewArgumentsException(String message) {
		super(message);
	}

}
