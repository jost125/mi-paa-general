package core;

class TooManyArgumentsException extends WrongArgumentsException {

	public TooManyArgumentsException(String message) {
		super(message);
	}

}
