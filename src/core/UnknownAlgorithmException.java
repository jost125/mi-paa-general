package core;

class UnknownAlgorithmException extends Exception {

	public UnknownAlgorithmException(String message) {
		super(message);
	}

}
