package core;

public class WrongArgumentsException extends Exception {

	public WrongArgumentsException(String string) {
		super(string);
	}

}
