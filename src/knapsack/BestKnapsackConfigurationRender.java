package knapsack;

import cz.cvut.felk.cig.jcop.problem.Configuration;
import cz.cvut.felk.cig.jcop.result.Result;
import cz.cvut.felk.cig.jcop.result.ResultEntry;
import cz.cvut.felk.cig.jcop.result.render.Render;
import java.io.IOException;

public class BestKnapsackConfigurationRender implements Render {

	@Override
	public void render(Result result) throws IOException {
		ResultEntry resultEntry = result.getResultEntries().get(0);
		Configuration bestConfiguration = resultEntry.getBestConfiguration();

		System.out.print("items: " + bestConfiguration.asList() + ";");
		System.out.print("best fitness: " + resultEntry.getBestFitness() + ";");
		System.out.print("cpu time: " + resultEntry.getStartTimestamp().getCpuTimeSpent(resultEntry.getStopTimestamp()) + ";");
		System.out.print("number of tested states: " + resultEntry.getOptimizeCounter());
	}

}
