package knapsack;

import core.CannotLoadInstanceException;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackInstance;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackTokenizer;
import java.io.BufferedReader;
import java.io.IOException;

public class KnapsackLoader {

	private KnapsackTokenizer knapsackTokenizer;

	public KnapsackLoader(KnapsackTokenizer knapsackTokenizer) {
		this.knapsackTokenizer = knapsackTokenizer;
	}

	public KnapsackInstance loadNextInstance(BufferedReader bufferedReader) throws CannotLoadInstanceException {
		try {
			String knapsackInstanceString = bufferedReader.readLine();
			KnapsackInstance knapsackInstance = null;
			if (knapsackInstanceString != null) {
				knapsackInstance = knapsackTokenizer.getInstance(knapsackInstanceString);
			}
			return knapsackInstance;
		} catch (IOException ex) {
			throw new CannotLoadInstanceException("Load of knapsack instance failed", ex);
		}
	}
}
