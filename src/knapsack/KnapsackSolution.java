package knapsack;

import java.util.List;

public class KnapsackSolution {
	private List<Integer> items;
	private Integer price;
	private Integer weight;

	public List<Integer> getItems() {
		return items;
	}

	public void setItems(List<Integer> items) {
		this.items = items;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "KnapsackSolution{" + "items=" + items + ", price=" + price + ", weight=" + weight + '}';
	}
}
