package knapsack;

import annealing.AnnealingItemsFinder;
import core.CannotLoadInstanceException;
import core.Loader;
import core.SolutionResolver;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackInstance;
import java.io.BufferedReader;
import java.util.Map;
import knapsack.branchbound.BBItemsFinder;
import knapsack.bfs.BFSItemsFinder;
import knapsack.dynamic.DynamicItemsFinder;
import knapsack.ftpas.FPTASItemsFinder;
import knapsack.heuristic.HeuristicItemsFinder;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class KnapsackSolutionResolver implements SolutionResolver {

	private Loader knapsackLoader;
	private BFSItemsFinder bfsItemsFinder;
	private HeuristicItemsFinder heuristicItemsFinder;
	private BBItemsFinder bbItemsFinder;
	private DynamicItemsFinder dynamicItemsFinder;
	private FPTASItemsFinder fptasItemsFinder;
	private AnnealingItemsFinder annealingItemsFinder;

	public KnapsackSolutionResolver(Loader knapsackLoader, BFSItemsFinder bfsItemsFinder, HeuristicItemsFinder heuristicItemsFinder, BBItemsFinder bbItemsFinder,
			  DynamicItemsFinder dynamicItemsFinder, FPTASItemsFinder fptasItemsFinder, AnnealingItemsFinder annealingItemsFinder) {
		this.knapsackLoader = knapsackLoader;
		this.bfsItemsFinder = bfsItemsFinder;
		this.heuristicItemsFinder = heuristicItemsFinder;
		this.bbItemsFinder = bbItemsFinder;
		this.dynamicItemsFinder = dynamicItemsFinder;
		this.fptasItemsFinder = fptasItemsFinder;
		this.annealingItemsFinder = annealingItemsFinder;
	}

	@Override
	public void run(BufferedReader bufferedReader, Map<String, Object> options) throws CannotLoadInstanceException {
		Logger.getRootLogger().setLevel(Level.OFF);

		KnapsackInstance knapsackInstance = (KnapsackInstance)knapsackLoader.loadNextInstance(bufferedReader);
		while (knapsackInstance != null) {
			System.out.print("instance: " + knapsackInstance.getId() + ";");
			if (options.containsKey("bfs")) {
				bfsItemsFinder.getSolution(knapsackInstance);
				System.out.print(";");
			}

			if (options.containsKey("heuristic")) {
				heuristicItemsFinder.getSolution(knapsackInstance);
				System.out.print(";");
			}

			if (options.containsKey("bb")) {
				bbItemsFinder.getSolution(knapsackInstance);
				System.out.print(";");
			}

			if (options.containsKey("dynamic")) {
				dynamicItemsFinder.getSolution(knapsackInstance);
				System.out.print(";");
			}

			if (options.containsKey("fptas")) {
				fptasItemsFinder.getSolution(knapsackInstance);
				System.out.print(";");
			}

			if (options.containsKey("ann")) {
				annealingItemsFinder.getSolution(knapsackInstance);
				System.out.print(";");
			}

			System.out.println("");
			knapsackInstance = (KnapsackInstance)knapsackLoader.loadNextInstance(bufferedReader);
		}
	}
}
