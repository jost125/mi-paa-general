package knapsack.bfs;

import cz.cvut.felk.cig.jcop.algorithm.Algorithm;
import cz.cvut.felk.cig.jcop.algorithm.graphsearch.bfs.BreadthFirstSearch;
import cz.cvut.felk.cig.jcop.problem.knapsack.Knapsack;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackInstance;
import cz.cvut.felk.cig.jcop.solver.SimpleSolver;
import cz.cvut.felk.cig.jcop.solver.Solver;
import knapsack.BestKnapsackConfigurationRender;

public class BFSItemsFinder {
	public void getSolution(KnapsackInstance knapsackInstance) {
		Knapsack knapsackProblem = new Knapsack(knapsackInstance);

		Algorithm algorithm = new BreadthFirstSearch();

		Solver solver = new SimpleSolver(algorithm, knapsackProblem);
		solver.addRender(new BestKnapsackConfigurationRender());
		solver.run();
		solver.render();
	}
}
