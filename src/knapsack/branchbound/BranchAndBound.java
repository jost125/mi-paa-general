package knapsack.branchbound;

import cz.cvut.felk.cig.jcop.algorithm.graphsearch.bfs.BreadthFirstSearch;
import cz.cvut.felk.cig.jcop.problem.Configuration;
import cz.cvut.felk.cig.jcop.problem.knapsack.Knapsack;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackItem;
import java.util.LinkedList;
import java.util.List;

public class BranchAndBound extends BreadthFirstSearch {

	@Override
	public void expand(Configuration configuration) {
		if (getOperations(configuration).size() > 1) {
			expandIfNotOverloaded(configuration);
		} else {
			super.expand(configuration);
		}
	}

	private int getFirstUncheckedIndex(Configuration configuration) {
		List<Integer> configurationVector = configuration.asList();
		int lastCheckedIndex = 0;
		for (int i = configurationVector.size() - 1; i >= 0; i--) {
			int vectorIndex = configurationVector.get(i);
			if (vectorIndex == 1) {
				lastCheckedIndex = i;
				break;
			}
		}
		return lastCheckedIndex + 1;
	}

	private void expandIfPromissing(double possiblePrice, Configuration configuration) {
		Knapsack knapsack = (Knapsack)problem.getProblem();
		List<KnapsackItem> knapsackItems = knapsack.getKnapsackItems();
		for (int i = getFirstUncheckedIndex(configuration); i < problem.getDimension(); i++) {
			possiblePrice += knapsackItems.get(i).getPrice();
		}
		if (possiblePrice >= bestFitness) {
			super.expand(configuration);
		}
	}

	private void expandIfNotOverloaded(Configuration configuration) {
		double possiblePrice = fitness.getValue(configuration);
		if (possiblePrice > 0) {
			expandIfPromissing(possiblePrice, configuration);
		}
	}

	private LinkedList getOperations(Configuration configuration) {
		return (LinkedList)configuration.getOperationHistory().getChronologicalList();
	}
}
