package knapsack.ftpas;

import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackInstance;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackItem;
import cz.cvut.felk.cig.jcop.util.PreciseTimestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import knapsack.KnapsackSolution;

public class FPTASItemsFinder {
	private int[][] table;
	private KnapsackInstance knapsackInstance;
	private List<KnapsackItem> knapsackItems;
	private int numberOfColumns;
	private int knapsackDimension;
	private int numberOfIgnoredBits;
	private int numberOfTestedStates;

	public FPTASItemsFinder(int ignoreNumberOfBits) {
		this.numberOfIgnoredBits = ignoreNumberOfBits;
	}

	public void getSolution(KnapsackInstance knapsackInstance) {
		this.knapsackInstance = knapsackInstance;
		this.knapsackItems = knapsackInstance.getKnapsackItems();
		this.knapsackDimension = knapsackItems.size();
		this.numberOfColumns = knapsackDimension + 1;
		this.numberOfTestedStates = 0;
		PreciseTimestamp start = new PreciseTimestamp();
		prepareTable();
		solve(0, 0, 0, 0);
		KnapsackSolution knapsackSolution = readSolutionFromTable();
		PreciseTimestamp stop = new PreciseTimestamp();

		System.out.print("items: " + knapsackSolution.getItems() + ";");
		System.out.printf("best fitness: %.1f;", (double)knapsackSolution.getPrice());
		System.out.printf("cpu time: %.1f;", (double)start.getCpuTimeSpent(stop));
		System.out.print("number of tested states: " + numberOfTestedStates);
	}

	private void prepareTable() {
		table = new int[getPriceSum() + 1][numberOfColumns];
	}

	private int getPriceSum() {
		int priceSum = 0;
		for (KnapsackItem knapsackItem : knapsackItems) {
			priceSum += knapsackItem.getPrice() >>> numberOfIgnoredBits;
		}

		return priceSum;
	}

	private void solve(int x, int y, int actualWeightSum, int itemIndex) {
		numberOfTestedStates++;
		if (!exceededCapacity(actualWeightSum)) {
			table[x][y] = table[x][y] == 0 ? actualWeightSum : Math.min(table[x][y], actualWeightSum);

			if (hasNext(itemIndex) && y + 1 < numberOfColumns) {
				KnapsackItem knapsackItem = knapsackItems.get(itemIndex);

				solve(x + (knapsackItem.getPrice() >>> numberOfIgnoredBits), y + 1, actualWeightSum + knapsackItem.getWeight(), itemIndex + 1);
				solve(x, y + 1, actualWeightSum, itemIndex + 1);
			}
		}
	}

	private boolean hasNext(int itemIndex) {
		return itemIndex < knapsackDimension;
	}

	private boolean exceededCapacity(int actualWeightSum) {
		return actualWeightSum > knapsackInstance.getCapacity();
	}

	private KnapsackSolution readSolutionFromTable() {
		KnapsackSolution knapsackSolution = null;

		for (int row = table.length - 1; row >= 0; row--) {
			if (table[row][knapsackDimension] <= knapsackInstance.getCapacity() && table[row][knapsackDimension] != 0) {
				knapsackSolution = new KnapsackSolution();
				List<Integer> solutionVector = reconstructVector(row);
				knapsackSolution.setPrice(getPriceOfVector(solutionVector));
				knapsackSolution.setWeight(table[row][knapsackDimension]);
				knapsackSolution.setItems(solutionVector);
				break;
			}
		}

		return knapsackSolution;
	}

	private List<Integer> reconstructVector(int solutionRow) {
		int row = solutionRow;
		ArrayList<Integer> solutionVector = new ArrayList<Integer>(Collections.nCopies(knapsackDimension, 0));
		for (int column = knapsackDimension - 1; column >= 0; column--) {
			if (table[row][column] != table[row][column + 1]) {
				solutionVector.set(column, 1);
				row -= (knapsackItems.get(column).getPrice() >>> numberOfIgnoredBits);
			} else {
				solutionVector.set(column, 0);
			}
		}

		return solutionVector;
	}

	private Integer getPriceOfVector(List<Integer> solutionVector) {
		Integer price = 0;
		for (int i = 0; i < knapsackItems.size(); i++) {
			price += solutionVector.get(i) == 1 ? knapsackItems.get(i).getPrice() : 0;
		}

		return price;
	}
}
