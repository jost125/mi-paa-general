package knapsack.heuristic;

import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackInstance;
import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackItem;
import cz.cvut.felk.cig.jcop.util.PreciseTimestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class HeuristicItemsFinder {
	public void getSolution(KnapsackInstance knapsackInstance) {
		PreciseTimestamp start = new PreciseTimestamp();

		List<Integer> itemList = null;
		double fitness = 0.0;

		for (int i = 0; i < 10000; i++) {
			Set<KnapsackItem> sortedKnapsackItems = new TreeSet<KnapsackItem>(new PricePerWeightComparator());
			sortedKnapsackItems.addAll(knapsackInstance.getKnapsackItems());

			List<KnapsackItem> solution = new ArrayList<KnapsackItem>();

			int totalWeight = 0;

			for (KnapsackItem item : sortedKnapsackItems) {
				if (totalWeight + item.getWeight() <= knapsackInstance.getCapacity()) {
					totalWeight += item.getWeight();
					solution.add(item);
				}
			}

			itemList = new ArrayList<Integer>();
			for (int j = 0; j < knapsackInstance.getKnapsackItems().size(); j++) {
				itemList.add(0);
			}

			fitness = 0.0;
			for (KnapsackItem item : solution) {
				fitness += item.getPrice();
				itemList.set(item.getIndex(), 1);
			}
		}

		PreciseTimestamp stop = new PreciseTimestamp();

		System.out.print("items: " + itemList + ";");
		System.out.printf("best fitness: %.4f;", fitness);
		System.out.printf("cpu time: %.4f;", start.getCpuTimeSpent(stop) / 10000.0);
	}
}
