package knapsack.heuristic;

import cz.cvut.felk.cig.jcop.problem.knapsack.KnapsackItem;
import java.util.Comparator;

public class PricePerWeightComparator implements Comparator<KnapsackItem> {
	@Override
	public int compare(KnapsackItem item1, KnapsackItem item2) {
		return -getPricePerWeight(item1).compareTo(getPricePerWeight(item2));
	}

	private Double getPricePerWeight(KnapsackItem item) {
		return new Double((double)item.getPrice() / (double)item.getWeight());
	}
}
